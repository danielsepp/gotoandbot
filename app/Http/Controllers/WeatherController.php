<?php

    namespace App\Http\Controllers;

    use Illuminate\Http\Request;

    class WeatherController extends Controller
    {

        private $apiKey = 'd2bdffb74f1a1fcc5687babf6a150c64';
        public $query = '';

        /**
         * WeatherController constructor.
         *
         * @param string $query
         */
        public function __construct(string $query)
        {
            $this->query = $query;
        }


        public function get()
        {

            $curl = curl_init();

            curl_setopt_array($curl, [
                CURLOPT_URL            => "http://api.openweathermap.org/data/2.5/weather?units=metric&cnt=1&appid=" . $this->apiKey . "&units=metric&q=" . $this->query,
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_ENCODING       => "",
                CURLOPT_MAXREDIRS      => 10,
                CURLOPT_TIMEOUT        => 30,
                CURLOPT_HTTP_VERSION   => CURL_HTTP_VERSION_1_1,
                CURLOPT_CUSTOMREQUEST  => "GET",
                CURLOPT_POSTFIELDS     => "",
            ]);

            $response = curl_exec($curl);
            $err      = curl_error($curl);

            curl_close($curl);

            if ($err) {
                return false;
            } else {
                return $response;
            }

        }

        public function reply () {

            $data = $this->get();

            $data = json_decode($data);

            if ( $data->cod == 404) {
                return "Honey, something went wrong. My friend told me, that " . $data->message;
            }

            $weather = reset($data->weather);
            $weather = $weather->description;

            return "Seems like we are gonna have " . $weather . " and around " . round($data->main->temp) . " degrees today with wind at " . $data->wind->speed . " m/s";

        }

    }
