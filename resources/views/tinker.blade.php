<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>Hello, job!</title>
    <link rel="stylesheet" href="{{ mix('css/app.css') }}">
</head>
<body class="chatbot">

<div id="app">
    <botman-tinker api-endpoint="/botman"></botman-tinker>
</div>

<script src="{{ mix('js/app.js') }}"></script>
</body>
</html>