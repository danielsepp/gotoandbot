<?php

    use App\Http\Controllers\BotManController;
    use BotMan\BotMan\Middleware\ApiAi;
    use App\Http\Controllers\WeatherController;

    $botman = resolve('botman');

    $dialogflow = ApiAi::create('6e5f30f031ed4b7aae5c775fc5d4145b')->listenForAction();
    $botman->middleware->received($dialogflow);


    $botman->hears('', function ($bot) {
        $extras = $bot->getMessage()->getExtras();

        if ($extras['apiAction'] == 'weather') {

            if (empty ($extras['apiParameters']['address'])) {
                return $bot->reply('I could not locate the place you asked about.');
            }

            $weatherController = new WeatherController(reset($extras['apiParameters']['address']));
            $reply             = $weatherController->reply();
            $bot->reply($reply);


        } else {
            if (empty($extras['apiReply'])) {
                //$bot->reply(json_encode($extras));
                $bot->reply('This functions has not been implemented yet. Daniel will surely work on this in the future, if need be. All requests that I am unable to process are logged and will be reviewed. So far I\'m not smarter than something like Weather in <city>. Sorry. ');
            } else {
                $bot->reply($extras['apiReply']);
            }
        }

    });

