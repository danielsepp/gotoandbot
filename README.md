## Running

1. Clone
2. Run ```composer install```
3. Probably need to run ```php artisan key:generate```
4. Run Homestead or Valet (optional)
5. Navigate to project's botman/tinker route in browser (i.e http://playbot.test/botman/tinker)
6. Try asking for weather or having small-talk.

## Changes

1. ```npm install```
2. ```npm run watch```

## License

Uses BotMan which is free software distributed under the terms of the MIT license.

